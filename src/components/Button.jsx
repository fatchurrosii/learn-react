function Button(props) {
    const { children, text, className = 'bg-blue-600' } = props;

    return (
        <button
            {...props}
            className={`${className} [&>svg]:h-5 [&>svg]:w-5 [&>svg]:stroke-1 flex items-center gap-x-2 text-white rounded-md px-4 py-2 font-medium text-lg`}>
            {text || children}
        </button>
    );
}

export default Button;
