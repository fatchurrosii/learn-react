import { IconBrandFacebook } from '@tabler/icons';
import Button from './components/Button';
import Card from './components/Card';

export default function App() {
    return (
        <div className='bg-slate-900 grid place-content-center min-h-screen text-slate-800 tracking-tight'>
            <div className='max-w-md w-full'>
                
                <Card>
                    <Card.Title>Hello React</Card.Title> 

                     <Card.Body>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus incidunt fuga temporibus sit facere repudiandae rerum,
                        itaque quam quia nisi quos laboriosam eum voluptatum nulla laudantium cumque exercitationem beatae minus debitis dolorum ullam
                        totam dolor nostrum! Dolores aperiam id officia illo quos optio voluptatem distinctio est quisquam, nisi molestiae nesciunt!
                    </Card.Body>

                    <Card.Footer>
                        {/* button component  */}
                        <Button className='bg-pink-600' onClick={() => console.log('hello react')} type='submit'>
                            <IconBrandFacebook />
                            Login
                        </Button>
                    </Card.Footer>
                </Card>

                {/* button componet with another write */}
                {/* <Button {...{ type: 'submit', onClick: () => console.log('hello react with another style') }}>
                    <IconBrandFacebook />
                    Login
                </Button> */}
            </div>
        </div>
    );
}

